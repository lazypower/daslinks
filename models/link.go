package models

import (
	"gopkg.in/mgo.v2/bson"

	"math/rand"
	"time"
)

const (
	// CollectionLink holds the name of the Links collection
	CollectionLink = "links"
	letters        = "1234567890abcdefghijklmnopqrstuvwyxABCDEFGHIJKLMNOPQRSTUVWXYZ-_"
)

type Link struct {
	Id        bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
	LongLink  string        `json:"longlink" form:"longlink" binding:"required" bson:"longlink"`
	ShortLink string        `json:"shortlink" bson:"shortlink"`
}

func randSeq(n int) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]byte, n)
	for i := range b {
		b[i] = letters[rand.Int63()%int64(len(letters))]
	}
	return string(b)
}

func randomGen(min, max int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max-min) + min
}

func (l *Link) GenerateShortLink() string {
	random := randomGen(4, 6)
	l.ShortLink = randSeq(random)
	return l.ShortLink
}
