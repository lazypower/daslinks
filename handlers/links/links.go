package links

import (
	"net/http"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"gitlab.com/lazypower/daslinks/models"
)

// new link
func New(c *gin.Context) {
	link := models.Link{}

	c.HTML(http.StatusOK, "links/form", gin.H{
		"title": "New Link",
		"link":  link,
	})
}

// create a link
func Create(c *gin.Context) {
	db := c.MustGet("db").(*mgo.Database)

	link := models.Link{}
	err := c.Bind(&link)
	if err != nil {
		c.Error(err)
		return
	}

	// Attach the short code to the link
	link.GenerateShortLink()

	err = db.C(models.CollectionLink).Insert(link)
	if err != nil {
		c.Error(err)
	}
	c.Redirect(http.StatusMovedPermanently, "/-/list")
}

func List(c *gin.Context) {
	db := c.MustGet("db").(*mgo.Database)
	links := []models.Link{}
	err := db.C(models.CollectionLink).Find(nil).Sort("-shortlink").All(&links)
	if err != nil {
		c.Error(err)
	}
	c.HTML(http.StatusOK, "links/list", gin.H{
		"title": "Links",
		"links": links,
	})
}

func Redirect(c *gin.Context) {
	db := c.MustGet("db").(*mgo.Database)
	link := models.Link{}
	err := db.C(models.CollectionLink).Find(bson.M{"shortlink": c.Param("_id")}).Sort("-shortlink").One(&link)
	if err != nil {
		c.Error(err)
	}
	if len(link.LongLink) == 0 {
		c.Redirect(http.StatusMovedPermanently, "/-/list")
	}
	c.Redirect(http.StatusMovedPermanently, link.LongLink)
}
