# DASLINKS

Yet another URL shortner written in Golang

## Usage

You'll need to run a mongodb service to back the datastore.

```
docker run -d --name mongo mongo
```

Grab the IP address of the Mongo container

```
docker inspect mongo
```

Run the url redirect webservice with the `MONGO_URL` env var exported to the `IP:PORT`
combo of the mongo container

```
MONGO_URL=172.17.1.5:27017 daslinks
```
