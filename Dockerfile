FROM golang

ADD . /go/src/gitlab.com/lazypower/daslinks
RUN go get -u github.com/golang/dep/cmd/dep
WORKDIR /go/src/gitlab.com/lazypower/daslinks
RUN dep ensure -v 
RUN go install gitlab.com/lazypower/daslinks

ENV MONGODB_URL ""
ENV PORT 8080

ENTRYPOINT /go/bin/daslinks

EXPOSE 8080
