package main

import (
	//"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/lazypower/daslinks/db"
	"gitlab.com/lazypower/daslinks/gin_html_render"
	"gitlab.com/lazypower/daslinks/handlers/links"
	"gitlab.com/lazypower/daslinks/middlewares"
)

const (
	Port = "8080"
)

func init() {
	db.Connect()
}

func main() {
	router := gin.Default()

	// Set html render options
	htmlRender := GinHTMLRender.New()
	htmlRender.Debug = gin.IsDebugging()
	htmlRender.Layout = "layouts/default"

	router.HTMLRender = htmlRender.Create()

	router.RedirectTrailingSlash = true
	router.RedirectFixedPath = true

	router.Use(middlewares.Connect)
	router.Use(middlewares.ErrorHandler)

	//router.Static("/public", "./public")

	// Routes

	/*router.GET("/", func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "/links")
	})*/

	/*router.GET("/new", links.New)
	router.GET("/links", links.List)
	router.POST("/links", links.Create)
	router.GET("/links/:_id", links.Redirect) */

	router.GET("/:_id", links.Redirect)
	router.GET("/:_id/list", links.List)
	router.POST("/:_id/new", links.Create)
	router.GET("/:_id/new", links.New)

	port := Port
	if len(os.Getenv("PORT")) > 0 {
		port = os.Getenv("PORT")
	}

	router.GET("/:_id/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	router.Run(":" + port)
}
